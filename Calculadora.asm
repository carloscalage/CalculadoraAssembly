;---------------------------------------------------
; Programa: Calculadora ICC
; Autor: Carlos Augusto e Clara Tigre
; Data: 21/07/2018
;---------------------------------------------------

X EQU 248                    
Y EQU 247                    
Z EQU 253
C EQU 246
A EQU 240

;INSTRUÇÕES INICIAIS

LDI 1
STA 253
LDI OP6
STA 255
LDI 6
STA 249
LDI RESET
STA 245
STA 244

INPUT:
 IN 1
 ADD 254
 JZ INPUT
 IN 0
 JMP @255

OP6:
 STA 251
 SUB 249
 JZ SAIR
 LDI 5
 SUB 251
 JZ OP5
 JN SAIR
 JMP CONTROL

OP5:
 LDI NUM2
 STA 255
 JMP INPUT


CONTROL:
 LDI NUM1
 STA 255
 JMP INPUT


MENU:
 LDA 251
 SUB Z
 JZ SOMA
 SUB Z
 JZ SUB
 SUB Z
 JZ MULT
 SUB Z
 JZ DIV
 SUB Z
 JZ FAT
 JNZ SAIR


NUM1:
 STA X
 LDI NUM2
 STA 255
 JMP INPUT
NUM2:
 STA Y
 JMP MENU


SOMA:
 LDA X
 ADD Y
 JMP @245


SUB:
 LDA X    
 SUB Y    
 JMP @245


MULT:
 LDI M2
 STA 245
 LDA X           
 STA C
 LDI 0
 M2:
  STA X
  LDA C
  SUB Z
  JN M3                       
  STA C                                     
  JMP SOMA
 M3:
  LDA X
  JMP @244


DIV:
 LDA X
 SUB Y
 STA X
 JZ DOUT
 JN DOUT
 LDA C
 ADD Z
 STA C
 JMP DIV
 DOUT:
  LDA C
  ADD Z
  JMP RESET


FAT:
 LDI F2
 STA 244
 LDA Y
 SUB Z
 STA X
 STA A
 JMP MULT
  F2:
   STA Y
   LDA A   
   SUB Z
   JZ F3
   STA X
   STA A
   JMP MULT
  F3:
   LDA Y
   JMP RESET


RESET:
 OUT 0
 LDI OP6
 STA 255
 LDI RESET
 STA 245
 LDI 0
 STA C
 JMP INPUT


SAIR:
 HLT

